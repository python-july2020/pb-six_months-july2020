from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Contact(models.Model):
    name = models.CharField(max_length=250,blank=True)
    email = models.EmailField(unique=True,blank=True)
    subject = models.CharField(max_length=250,blank=True)
    message = models.TextField(blank=True)
    received_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Register_Table(models.Model):
    opt = (
        ("male", "Male"),
        ("female","Female"),
        ("others","Others")
    )
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    gender = models.CharField(max_length=20, choices=opt)
    is_verified = models.BooleanField(default=False)
    phone_number = models.CharField(max_length=100, blank=True, null=True)
    profile_pic = models.ImageField(upload_to='profile', null=True, blank=True)
    last_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username

class Category(models.Model):
    name = models.CharField(max_length = 250)
    image = models.ImageField(upload_to="cat/%Y/%m/%d", null=True, blank=True)
    description = models.TextField(blank=True)
    uploaded_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class Song(models.Model):
    uploaded_by = models.ForeignKey(Register_Table, on_delete=models.CASCADE, null=True)
    song_name = models.CharField(max_length=250)
    song_category = models.ForeignKey(Category, on_delete = models.CASCADE)
    artist_name = models.CharField(max_length= 250)
    song_file = models.FileField(upload_to="songs/%Y/%m/%d")
    uploaded_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __str__(self):
        return self.song_name   

class donors(models.Model):
    name = models.CharField(max_length=250)
    contact = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    amount = models.FloatField()
    description = models.TextField()
    status = models.BooleanField(default=False)
    uploaded_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


