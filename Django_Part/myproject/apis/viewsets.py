from rest_framework.response import Response
from rest_framework.decorators import api_view
from apis import serializers
from rest_framework import status
from myapp.models import Contact, Song, Category

@api_view(["GET","POST","PUT","DELETE"])
def contact_api(request):
    if request.method=="GET":
        if "id" in request.GET:
            id = request.GET.get("id")
            details = Contact.objects.filter(id=id)
            # details = Contact.objects.filter(name__contains=id)
        else:
            details = Contact.objects.all()

        serializer = serializers.ContactSerializer(details, many=True)

        dt = {
            "message":"{} records found!".format(len(serializer.data)),
            "data":serializer.data,
        }
        return Response(data=dt, status=status.HTTP_200_OK)

        # return Response(data=serializer.data, status=status.HTTP_200_OK)
        # return Response(data=serializer.data, status=HTTP_404_NOT_FOUND )
    
    elif  request.method == "POST":
        all_data = request.data        
        res=serializers.ContactSerializer(data=all_data)
        if res.is_valid():
            res.post()
            return Response(data={"message":"Thanks for your feedback!","data":all_data}, status=status.HTTP_201_CREATED)
        else:
            return Response({"error":res.errors},status=status.HTTP_400_BAD_REQUEST)

    elif  request.method == "PUT":
        all_data = request.data  
        res=serializers.ContactSerializer(data=all_data)
        if res.is_valid():
            try:
                con_ob = Contact.objects.get(id=all_data.get("id"))
                con_ob.name = all_data.get("name")
                con_ob.email = all_data.get("email")
                con_ob.subject = all_data.get("subject")
                con_ob.message = all_data.get("message")
                con_ob.save()
                return Response({"status":"success","message":"Updated Successfully!"}) 
            except:
                return Response({"status":"error","message":"Please check ID again!"}) 
        else:
            return Response({"status":"error","message":res.errors})

    elif request.method == "DELETE":
        try:
            id = request.data.get("id")
            Contact.objects.get(id=id).delete()
            return Response({"status":"success","message":"Deleted Successfully!"})
        except:
            return Response({"status":"error","message":"Could not delete! Please check 'id' wisely!"})


@api_view(["GET",])
def songs_api(request):
    # query = Song.objects.filter(singer_name__contains="Karan")
    query = Song.objects.all().order_by("-id")
    all_songs = serializers.SongSerializer(query,many=True)
    return Response(all_songs.data)    