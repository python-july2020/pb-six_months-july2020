from django.urls import path
from class_based_app import views

app_name = "class_based_app"

urlpatterns = [
    path("contact/", views.ContactView.as_view(), name="contact"),
    path("category_list/", views.CategoryList.as_view(), name="category_list"),
    path("category_detail/<int:pk>/", views.CategoryDetail.as_view(), name="categorydetail"),
    path("category_create/", views.CategoryCreate.as_view(), name="categorycreate"),
    path("category_delete/<int:pk>/", views.CategoryDelete.as_view(), name="categorydelete"),
]