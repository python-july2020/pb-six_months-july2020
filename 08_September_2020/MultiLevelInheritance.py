class GrandParent:
    a = 10
    def fun1(self):
        print("Member function of grand parent")


class Parent(GrandParent):
    b = 20
    def fun2(self):
        print("Member function of Parent class")


class child(Parent):
    c = 30
    def fun3(self):
        super().fun1()
        super().fun2()
        print("Member function of Child class")

class main(child):
    pass

obj = child()
# print(obj.a)
# print(obj.b)
# obj.fun1()
# obj.fun2()
# print(obj.c)
obj.fun3()
