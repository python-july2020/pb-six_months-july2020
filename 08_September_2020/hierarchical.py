class Student:
    clz = "My College of Information & Technology"
    def __init__(self, name, rno):
        self.n = name
        self.r = rno


class Library(Student):
    def book_details(self, bname, isdt, rtdt):
        print("Here are book details of: ", self.n, self.clz)
        print("Book Name: {}\nIssued On: {}\n Return date:{}\n".format(bname, isdt, rtdt))

class Account(Library):
    def fee_details(self, total, pending):
        print("FEE DETAILS OF: ", self.n, self.clz)
        print("Total Fee: Rs.{}/-\nPending Fee:Rs.{}/-\n".format(total,pending))


obj = Library("Amandeep Kaur", 1002)
obj1 = Library("Peter Parker", 1003)
obj1.book_details("C++", "08/09/2020", "18/09/2020")

s1 = Account("Jack Atkinson", 1004)
s1.fee_details(100000, 10000)
