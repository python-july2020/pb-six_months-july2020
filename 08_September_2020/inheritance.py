class XYZ:
    pass
class Student:
    clz = "XYZ Engg. College"
    def intro(self,name):
        print("Hi this is {} i am student of {}".format(name, self.clz))

#Single Inheritance
class Library(Student):
    clz = "ABCD COLLEGE OF MANAGEMENT"
    def book_details(self,bname, rtdate):
        super().intro("Harry Potter")
        print("COOLEGE NAME: ", super().clz)
        print("Book Name:{}\nReturn Date:{}\n".format(bname, rtdate))

obj = Library()
# print(obj.clz)
# obj.intro("Amandeep Kaur")
obj.book_details("Python Programming", "20/9/2020")
print(isinstance(obj, XYZ))
# s1 = Student()
# print(s1.clz)
# s1.intro("Peter")
# s1.book_details("ABCD", "10/09/2020") ## Parent class can't access child's attributes