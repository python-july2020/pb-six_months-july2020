from lib import MyError

try:
    a = 1
    b = 20
    if a>b:
        print("Result: ",a-b)
    else:
        raise MyError

except MyError as ab:
    print("ab=",ab)
    print("OOPS somethong went wrong!")


try:
    print(hello)
except NameError as ab:
    print(ab)