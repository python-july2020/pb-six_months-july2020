st = "My name is Amandeep Kaur "
ab = "Hello There"
cd = "10"

## Concatenate Operator
print(st+ab)

## Membership Operator

print("name" in st)
print("name" not in st)

if "Aman" in st:
    print("Person found!")
else:
    print("Person not found!")

### String methods

print(st)
print(st.split())
print(st.split("n"))
print(st.split("A"))