intro = "Programming Language" ## Python string is an immutable type, we can't insert, delete elements in it
colors = ["red","green","blue","cyan","magenta","black","orange"] #Python list is an mutable type

# for i in intro:
#     print(i)

# colors[0] = "A"

# print(colors)

# for i in range(10):
#     print("i=",i)

# for i in range(10,20,2):
#     print(i)

# for i in range(0,110,10):
#     print(i)

# for i in range(len(colors)):
#     print("colors[{}] = {}".format(i, colors[i]))
#     # print("colors[",i,"]=",colors[i])

for i in range(10,0,-1):
    print(i*"*")