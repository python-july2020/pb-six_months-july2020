ab = "thIS is A stRing In PytHon"
sub = "PYTHON"
num = "987654"

print(ab.upper())
print(ab.lower())
print(ab.capitalize())
print(ab.title())
print(ab.swapcase())
print(ab)

print(sub.isupper())
print(sub.islower())
print(sub.isalpha())
print(sub.isdigit())
print(num.isdigit())

### To check built-in functions

print(dir(ab))