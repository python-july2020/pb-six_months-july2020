while True:
    num = input("Please Enter Your Account Number: ")
    if num=="0":
        break
    if num.isdigit():
        if len(num)>=10:
            print("Account Number: {}".format(len(num[:-3])*"*"+num[-3:]))
            break
        else:
            print("Number must contain atleast 10 digits")

    else:
        print("Account Number must contain digits only!")