student = {
    "name":"Peter Parker",
    "roll_no":1234,
    "subjects":["CN","DBMS","CG"],
    "is_present":True,
    "hobbies":("Reading","Singing"),
    "others":{
        "languages":("English","Hindi"),
        "age":23
    }
}

# # print(student)
# # print(type(student))
# print(len(student))

# ### Accessing elements
# print(student["name"])

# print(student["subjects"][1])

# print(student["roll_no"])

# print(student["others"]["languages"][1])



## Looping

for i in student:
    print(i,"=>" ,student[i])
