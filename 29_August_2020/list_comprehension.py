
ab = []
for i in range(1,101):
    if i%8==0:
        ab.append(i)
    

# print(ab)

### Using list comprehension
cd = [x for x in range(1,101) if x%8==0]
# print(cd)

#############################################################################

marks = [100,33,5,33,18,87,34,99]

result=["pass" if i>33  else "fail" for i in marks]
print(result)

#############################################################################

# nums = ["odd" if i%2==1 else "even" for i in range(100)]
# print(nums)

# nums = []
# for i in range(100):
#     if i%2==1:
#         nums.append("odd")
#     else:
#         nums.append("even")
# print(nums)

##############################################################################

# ls=[]
# for i in range(1,101):
#     ls.append(i)


# abcd = ["hello" for j in range(1,101)]
# abcd = [j for j in range(1,101)]
# print(abcd)

##########################################################################