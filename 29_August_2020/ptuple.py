ab = 10,20,30

print(type(ab))
print(ab)

## Indexing

print(ab[0])
print(ab[-1])

colors = ("red","black","cyan","purple","orange")

## Slicing

print(colors[2:])
print(colors[:2])
print(colors[-2:])


## Step Size
print(colors[::-1])
print(colors[::2])

### Operators

print(ab+colors)
print(ab*3)
print(10 in ab)
print(10 not in ab)


# colors[0]="white" ## Python tuple is immutable, we can'y add, delete,update elements once created
# del colors[0]
# del colors

print(colors)
print(dir(colors))

print(len(colors))
print(colors.count("red"))
print(colors.index("red"))

