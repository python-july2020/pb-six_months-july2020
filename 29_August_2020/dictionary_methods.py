movie = {
    "movie_name":"PK",
    "actor_name":"Amir Khan",
    "year":2016,
    "turnover":"10 Crores",
    "actress":"Anushka Sharma",
}

### Insert Elements
movie["language"] = "Hindi"
movie.update({"platform":"Cinemas"})


## Update Element
movie["language"] = "English"
movie.update({"platform":"OT"})
movie["ratings"] = ""

# movie.pop("turnover")
# movie.pop("actor_name")
# movie.popitem()
# movie["language"]=""
del movie["actress"]

# for i in movie:
#     print(i,"=>",movie[i])


# print(movie.keys())
# print(movie.values())


# for k,v in movie.items():
#     print(k,v)


fooditem = ["name", "price", "type", "image"]
d= {}.fromkeys(fooditem, "No Value yet")

d["name"] = "Burger"
print(d)


