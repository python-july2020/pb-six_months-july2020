f = open("country.html", "a")

import requests

con = input("Enter Country Name: ")

data = requests.get("https://restcountries.eu/rest/v2/all").json()
flag = False
rs = ""
for i in data:
    if i["name"].lower()==con.lower():
        rs += "<div style='width:20%;margin:1%; padding:1%; box-shadow:0px 0px 10px gray;float:left;'>"
        rs += "<h2>{}</h2>".format(i["name"])
        rs += "<img src='{}' style='height:200px;width: 100%;'>".format(i["flag"])
        rs += "<p>Population: {}</p>".format(i["population"])
        rs += "<p>Region{}</p>".format(i["region"])
        rs += "</div>"
        flag = True
print(rs)

if flag==False:
    print("No results for {}".format(con))

f.write(rs)
print("File written successfully!!!")