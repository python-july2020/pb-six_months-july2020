import requests

con = input("Enter Country Name: ")

data = requests.get("https://restcountries.eu/rest/v2/all").json()
flag = False
for i in data:
    if i["name"].lower()==con.lower():
        flag = True
        print("\nName: {}".format(i["name"]))
        print("Capital: {}".format(i["capital"]))
        print("Population: {}".format(i["population"]))
        print("Area: {}".format(i["area"]))
        print("Region: {}".format(i["region"]))
        print("Borders: {}".format(i["borders"]))
        lg = ""
        
        for j in i["languages"]:
            lg+= j["name"]+", "
        print("Languages: {}".format(lg))
        print("Flag: {}".format(i["flag"]))
        print("===================================== \n")

if flag==False:
    print("No results for {}".format(con))