s1 = {10,20,30,40}
s2 = {100, 20, 50, 10}
s3 = {"Aman", "Jack", "Aman"}

## Union 
# un = s1|s2
un = s1.union(s3)
print(un)


## Intersection

# ab = s1 & s2
ab = s1.intersection(s2)
print(ab)

##  Difference
# diff = s1-s2
diff = s2.difference(s1)
print(diff)

## Symmetric difference
sym = s1.symmetric_difference(s2)
print(sym)