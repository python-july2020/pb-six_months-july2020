a = frozenset({10,20,30, 20})
print(a) ## frozenset are immutable by nature
# a.add(7)

c = {10,4,6,7,4,5}
c.pop()
c.remove(6)
print(c)


# roll = (10,3,2,4,563,2,3,56,2,10,30,2,2,10,87)
# unique=list(set(roll))

# print(unique)


languages = frozenset({"C++", "C", "Python", "Ruby", "C", "PHP", "Python"})
# languages.add("Java") ## frozen sets are immutable
# languages.pop()   
print(languages)