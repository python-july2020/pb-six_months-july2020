#global variable

a = "Hello"

def myfunc():
    # local variable
    ab = "There"
    global a
    a += " World!"

myfunc()
print("a = ", a)
