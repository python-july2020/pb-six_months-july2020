def mul(a,b,c):
    # Python comment
    '''
        This function accepts three parameters
        and multiply them with each other
        Exammple:

        mul(10,10,10) = 1000
    '''
    print(a*b*c)
    return 0

# print(mul.__doc__)

def hello():
    ''' This is a demo function in python '''

    print("Functio called")

print(hello.__doc__)
print(len.__doc__)
print(print.__doc__)