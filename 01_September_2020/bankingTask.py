import random 
options = """
    Press 1: To create Account
    Press 2: To Check balance
    Press 3: To deposit
    Press 4: To withdraw
    Press 0: To Exit
"""
print(options)
users = []
def create_account():
    name = input("Enter your name: ")
    bal = int(input("Enter Initial Amount: "))
    acc = random.randint(1000,9999)
    user = {
        "name":name,
        "balance":bal,
        "account_no":acc,
    }
    users.append(user)
    print("Dear {}, Your account created successfully , Here is Acc. no. {}\n".format(name, acc))



def check_balance():
    num = int(input("Enter Your Account Number: "))
    match = False
    for i in users:
        if i["account_no"] == num:
            match = True
            print("Welcome {}!!!".format(i["name"]))
            print("Your Balance: Rs.{}/-\n".format(i["balance"]))

    if match==False:
        print("Sorry you are not a registered customer!!!\n")

def deposit():
    num = int(input("Enter Your Account Number: "))
    match = False
    for i in users:
        if i["account_no"] == num:
            match = True
            print("Welcome {}!!!".format(i["name"]))
            amount = float(input("Enter Amount to Deposit: "))
            i["balance"] += amount 
            print("Dear {}, your account credited with Rs.{}/- Current Balance: Rs.{}/-\n".format(i["name"], amount, i["balance"]))

    if match==False:
        print("Sorry you are not a registered customer!!!\n")

def withdraw():
    num = int(input("Enter Your Account Number: "))
    match = False
    for i in users:
        if i["account_no"] == num:
            match = True
            print("Welcome {}!!!".format(i["name"]))
            amount = float(input("Enter Amount to Withdraw: "))
            if i["balance"] >= amount:
                i["balance"] -= amount 
                print("Dear {}, your account debited with Rs.{}/- Current Balance: Rs.{}/-\n".format(i["name"], amount, i["balance"]))
            else:
                print("You have no sufficient balance to complete this transaction!!!\n")
                
    if match==False:
        print("Sorry you are not a registered customer!!!\n")



while True:
    op = input("Enter Your choice: ")
    if op=="1":
        create_account()
    elif op=="2":
        check_balance()

    elif op=="3":
        deposit()

    elif op=="4":
        withdraw()

    elif op=="0":
        break
    else:
        print("Invalid choice!")