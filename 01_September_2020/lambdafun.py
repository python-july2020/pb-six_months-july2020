def square(x):
    return x**2


result = lambda x:x**2

add = lambda x,y:x+y

print(result(2))
print(result(10))
print(result(8))

print(add(199,32))
print(add(10,10))

## WAP to check whether a number is even or not

def check(a):
    if a%2==0:
        return True 
    else:
        return False 

# print(check(87))
# print(check(8))

# check = lambda x:x%2==0

check = lambda x:"even" if x%2==0 else "not even"

print(check(89))
print(check(8))
print(check(11))


    