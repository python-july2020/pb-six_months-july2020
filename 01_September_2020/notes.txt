Some Builtin Functions

filter()
    Syntax
        list(filter(function, sequence))

    - Search for specific values in a sequence (for which function returns true)

map()

reduce()