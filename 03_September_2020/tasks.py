# print(chr(65))
# print(chr(66))
# print(chr(97))
# print(chr(98))

n = int(input("Enter Number: "))

for i in range(1, n+1):
    
    for j in range(n, i, -1):
        print("",end=" ")
    for k in range(1, i+1):
        if (k==i) or (k==1) or (i==n):
            print("*", end=" ")
        else:
            print("+", end=" ")
    print()

# n = 5, i=1 = 4 spaces   1 star
# n = 5, i=2 = 3 spaces   2 stars
# n = 5, i=3 = 2 spaces   3 stars
# n = 5, i=4 = 1 spaces   4 stars
# n = 5, i=5 = 0 spaces   5 stars

# for i in range(1, n+1):
#     for j in range(i, 0, -1):
#         print(chr(64+j  ), end=" ")
#     print()

# for i in range(65+n, 64, -1):
#     for j in range(65, i+1):
#         print(chr(i), end=" ")
#     print()


# for i in range(1, n+1):
#     for j in range(1,i+1):
#         print(i, end=" ")
#     print()
    
# n = int(input("Enter Number: "))
# for i in range(1, n+1):
#     for j in range(1,i+1):
#         print(j, end=" ")
#     print()
    
# n = int(input("Enter Number: "))
# for i in range(1, n+1):
#     for j in range(1,i+1):
#         print("*", end=" ")
#     print()
    

### Factorial

# fact=1
# num = int(input("Enter Number: "))
# for i in range(num, 1, -1):
#     fact *= i

# print("Factorial of {} is: {}".format(num, fact))