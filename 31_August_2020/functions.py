# def hello(): #definition
#     print("statement 1")
#     print("Function code executed!!!") #body

# hello() #calling


def abcd():
    ab = "hello"
    cd = "there"
    return 100


print(abcd())
print(abcd()*2)
print(abcd()+100)

print(ab)