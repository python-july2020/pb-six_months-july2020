def Area(rad=0, pi=3.14):
    print("{} * {} **2 = {}".format(pi, rad, pi*rad**2))

Area(10, 3.14)

Area(1)
Area(2)

Area()

Area(1,1)