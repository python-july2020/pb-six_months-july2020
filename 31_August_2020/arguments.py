def student(name, roll, email):
    print("Name: {} Roll No.: {} Email: {}".format(name, roll, email))

student("Aman", 1001, "aman@gmail.com")

# Positional Arguments
student(1002, "Peter","peter1@gmail.com")

# Keyword Arguments
student(roll=1002, name="Peter",email="peter1@gmail.com")

student("Jack", 1004,  email="jack@gmail.com")




