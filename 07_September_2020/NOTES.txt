OOPS - Object Oriented Programming

OOPS Concepts:

1. object
2. class 
3. Inheritance
4. Constructors & Destructors 
5. Encapsulation
6. Polymorphism
    Method Overloading 
    Method Overriding 
7. Data Abstraction

Object is a real world entity. Object contains:
    - properties
    - behaviour

Class is a logical entity. We can create multiple objects from one class. 
    - data memebers 
    - member functions

Constructor 
    - Constructor is a special type of function, which automatically calls on object creation
    - It is use to create resources, (initialize variables)
    - Constructor must return None 

Destructor
    - Destructor is a special type of function, which automatically calls on object creation
    - It is used to destroy resources occupied by Constructor


