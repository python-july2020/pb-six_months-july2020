
class Student:
    clz = "XYZ Engg. College"

    def intro(self, name, rno):
        self.name = name
        # print("Name: {}\nRoll No:{}\nCollege:{}".format(name, rno, self.clz))

    def fee_details(self, total, pending):
        print("Here are fee details of: ", self.name)
        print("Total Fee:Rs.{}/-\nPending Fee:Rs.{}/-".format(total,pending))

    def welcome(self):
        print("WELCOME {}!!! TO COLLEGE".format(self.name))

s1 = Student()
s2 = Student()

s1.intro("Amandeep Kaur", 1001)
s2.intro("James", 1002)

s2.fee_details(10000, 0)
s1.fee_details(10000, 500)

s2.welcome()
s1.welcome()


















# def abcd(x,y):
#     print(x+y)

# def xyz():
#     print("value of x is: ", x)

# abcd(10,20)
# xyz()


