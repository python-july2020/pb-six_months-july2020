## Creating a class
class Student:
    #data members 
    clz = "Abcd Engg. College"

    #member function
    def intro(self):
        print("Member Function of Student Class")
    
    def detail(self,name):
        print("My Name is {} ".format(name))


## Creating an object
obj = Student()

print(obj.clz)
obj.intro()
obj.detail("Aman")