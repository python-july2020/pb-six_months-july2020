class Student:
    clz = "ABCD College of Management"
    def __init__(self, name, rno, age):
        self.nm = name
        self.a = age
        print(name, "registred successfully")
        # def abcd():
        #     print("INNER FUNCTION")
        # abcd()

    def book_details(self, bname, rdate):
        print("\n{} issued a book today!!!".format(self.nm))
        print("book Name:{}\nReturn Date:{}\n".format(bname, rdate))

    def __del__(self):
        
        del self.nm, self.a
       

        print(self.nm)
        print("Destructor Called!!!")
        
s1 = Student("Peter Parker", 1002, 22)
s2 = Student("Schinchan Nohara", 1003, 5)

s2.book_details("AI", "21-09-2020")
s1.book_details("Python Programming", "22-09-2020")

print(s1.clz)
















# class Student:

#     def __init__(self): #Non-parametrized Constructor
#         self.name = "Amandeep Kaur"
#         print("Constructor Called!!!")

#     def abcd(self):
#         print(self.name)

# s1 = Student()
# s2 = Student()

# s1.abcd()

