op = """
    Press 1: To Add user
    Press 2: To view users
    Press 3: To Remove User 
    Press 0: To Exit
"""
print(op)
users=[]
while True:
    inp = input("Enter Your Choice: ")
    if inp=="1":
        nm = input("Enter User name to Add: ")
        users.append(nm)
        print(nm, "added successfully!!\n")

    elif inp=="2":
        print("Total Users: ", len(users))
        c=0
        for i in users:
            c+=1
            print("{}: {}".format(c,i))

        print("\n")
    
    elif inp=="3":
        nm = input("Enter User name to Remove: ")
        count = users.count(nm)
        if count != 0:
            users.remove(nm)
            print(nm,"removed successfully!!!\n")
        else:
            print("User not foun in record!!\n")

    elif inp=="0":
        break 

    else:
        print("Invalud Choice!!!\n")

        