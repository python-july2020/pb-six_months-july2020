colors = ["red","green","magenta","black","white","yellow"]

print(colors)
print(len(colors))

### Indexing 
print(colors[2])
print(colors[-3])

## Slicing
print(colors[-2:])
print(colors[3:])
print(colors[1:5])

print(colors[1:5][::-1])

## Step Size
print(colors[::2])
print(colors[::-1])

# print("1"*10)
# print("2"*10)
# print("*"*10)
# print("Aman"*4)
