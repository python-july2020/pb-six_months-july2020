group1 = ["Aman","Peter","James","Jack"]


### Insert Element
# group1.append("Potter")
# group1.insert(0,"nick")


## Update Element
# group1[1] = "Peter Parker"
# group1[0] = "Amandeep Kaur"

## Delete Element

print("Before:",group1)
# del group1[1]

# group1.pop()
# group1.pop(0)
group1.remove("Peter")

print("After:",group1)




