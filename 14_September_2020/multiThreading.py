import threading
import time

def myfun():
    for i in range(5):
        # time.sleep(1)
        print("Value of i is", i)

def abcd(x,y):
    for i in range(5):
        # time.sleep(1)
        print("Hello There: {} + {} = {}".format(x,y,x+y))


t1 =threading.Thread(target=myfun)
t2 =threading.Thread(target=abcd, args=(10,20))

t1.start()
t2.start()

t1.join()
t2.join()

print("I AM MAIN THREAD")
print("Hii Everyone!!!")

