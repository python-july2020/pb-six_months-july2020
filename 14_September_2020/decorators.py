# A decorator is a callable object (special type of python functions) which are used to modify a function

def outer_func(fun):
    def inner():
        print("This is a inside function")
        ch = fun()
        if ch==True:
            print("Profile Details: Welcome to Dashboard")
        else:
            print("You are not an authorized user!!!")

    return inner

@outer_func
def check():
    # print("User is logged in!!!")
    return False
    
@outer_func
def welcome():
    for i in ["Aman", "Peter", "Jack", "James"]:
        print("Welcome: ", i)
    return True

welcome()


# result = outer_func(check)
# result()

# check()
