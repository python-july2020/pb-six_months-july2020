b = "This is a string in python"
a = "0123456789"

## Step Size
print(a[::1])
print(a[::2])
print(a[::3])
print(a[::-1])
print(a[::-1])

# ## Slicing 
# print(a[2:])
# print(a[-3:])

# print(a[:2])
# print(a[:-2])

# print(a[2:6])
# print(a[-5:-2])

## Indexing
# print(a[0])
# print(a[1])
# print(a[2])
# print(a[4])

# print(a[-1])
# print(a[-2])
# print(a[-3])

# print(len(a))

##last element
# print(a[len(a)-1])