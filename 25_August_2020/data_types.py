a = 10
b = 10.5
c = "Hello"
d = 10+5j
e = [10,20,30]
f = True
g = {"name":"Aman","roll_no":123, 0:10} 
h = {10}
i = (10,20)
j = "red","green","blue"

print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))
print(type(f))
print(type(g))
print(type(h))
print(type(i))
print(type(j))