from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse("<h1 style='color:green;'>WELCOME TO MY SITE</h1>")

def home(request):
    return HttpResponse("<h1 style='text-align:center; color:red;font-size:40px;'>IT IS VERY FIRST PAGE OF MY SITE</h1>")

def about(request):
    
    colors = ["red", "green", "yellow", "black","#00ff00", "magenta", "orangered"]
    abcd  = {"col":colors,"name":"My Name is Amandeep Kaur"}
    return render(request, "about.html", abcd)

    # name,capital, population, borders, flag