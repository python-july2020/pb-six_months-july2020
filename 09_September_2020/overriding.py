class Parent:
    a = "Hello"
    def __init__(self):
        print("Constructor 1")

    def abcd1(self):
        print("PArent Member Function")

class Child(Parent):
    a = "There"
    def __init__(self):
        super().__init__()
        print("Constructor 2")
        
    def abcd(self):
        print("Child Member Function")

    obj = Child()
    obj.abcd()
    print(obj.a)
