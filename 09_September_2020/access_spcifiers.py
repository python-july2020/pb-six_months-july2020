class Employee:
    company_name = "Abcd Tech Pvt Ltd" #public
    __pincode = 100098773 #Private data member
    
    def __myfun(self): #Private Member function
        print("MY FUNCTION CALLED!!!", self.__pincode)

    def hello(self):
        self.__myfun()
        print("Welcome To our company: ")
        print("PIN: ", self.__pincode)

class Child(Employee):
    def __init__(self):
        # super().__myfun() ## Child object can't use private member function of parent class
        print("Inside Child: ",self.company_name)
        # print("PIN CODE: ", self.__pincode  ) ## Child object can't use private data members of parent class

obj = Employee()
obj.company_name = "SachTech SOlution Pvt. Ltd."
print(obj.company_name)
# obj.hello()
ab = Child()

# obj.__myfun()