
try:
    print("Hello World")
    a = 10
    print("End of statement")
    # b = a/0
    b = a+"10"
    import hello

except NameError:   
    print("Please check variable names carefuly")

except ModuleNotFoundError:
    print("Please Check module names!!! ")

except:
    print("OOPs Something went wrong!!!")


try:
    name = input("ENTER NAME: ")
    print(name)
    print(name+10)

except:
    print("Something wrong happening there!!!")